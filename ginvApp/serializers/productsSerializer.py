from ginvApp.models.account import Account
from rest_framework import serializers

from ginvApp.models.products import Products

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['sku_id','descripcion','cantidad','costo_unidad','costo_total']

        