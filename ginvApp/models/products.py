from django.db import models

class Products(models.Model):
    sku_id = models.CharField(max_length = 60, primary_key = True )
    descripcion = models.CharField(max_length = 100)
    cantidad = models.IntegerField()
    costo_unidad = models.IntegerField()
    costo_total = models.IntegerField()