from django.apps import AppConfig


class GinvappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ginvApp'
