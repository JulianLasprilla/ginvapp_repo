from rest_framework import generics

from ginvApp.serializers import ProductsSerializer
from ginvApp.models import Products


class ProductsCreateView (generics.CreateAPIView):
        queryset = Products.objects.all()
        serializer_class = ProductsSerializer