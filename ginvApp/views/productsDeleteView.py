from rest_framework import generics

from ginvApp.serializers import ProductsSerializer
from ginvApp.models import Products

class ProductsDeleteView (generics.DestroyAPIView):
        queryset = Products.objects.all()
        serializer_class = ProductsSerializer