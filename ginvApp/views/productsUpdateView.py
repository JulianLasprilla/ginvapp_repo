from rest_framework import generics
from rest_framework.serializers import Serializer

from ginvApp.serializers import ProductsSerializer
from ginvApp.models import Products


class ProductsUpdateView (generics.RetrieveUpdateAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductsSerializer
    