from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .productsListView import ProductsListView
from .productsCreateView2 import ProductsCreateView
from .productsUpdateView import ProductsUpdateView
from .productsDeleteView import ProductsDeleteView
